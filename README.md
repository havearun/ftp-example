# Deploy via FTP

An [example script](https://bitbucket.org/ian_buchanan/ftp-example/src/master/bitbucket-pipelines.yml)
for triggering FTP-based deployment from Bitbucket Pipelines.

## How to use it

* Add required environment variables to your Bitbucket enviroment variables.
* Copy `bitbucket-pipelines.yml` to your project.

## Required environment variables

* **`FTP_TARGET_PATH`**: (Required) Path on the remote server to place the files. Example: `\`
* **`FTP_PORT`**: (Required) The FTP port on the target server. Example: `21`
* **`FTP_USERNAME`**: (Required) The username for authentication on the target server. Example: `anonymous`
* **`FTP_PASSWORD`**: (Required) The password for authentication on the target server. Pro-tip: use the lock symbol to encrypt and mask the password.
* **`FTP_TARGET_SITE`**: (Required) URL of the remote server, including the protocol. Example: `ftp://127.0.0.1`

## Deploy via the lftp client

```
lftp -d -e "mirror --reverse . $FTP_TARGET_PATH" -p $FTP_PORT -u $FTP_USERNAME,$FTP_PASSWORD $FTP_TARGET_SITE
```

* Uses `-d` switch to help with debugging. See [lftp man page](https://linux.die.net/man/1/lftp) for more docs.
* lftp can handle several file access methods - ftp, ftps, http, https, hftp, fish, sftp and file

## Build configuration

```
image: cschlosser/alpine-lftps
pipelines:
  branches:
    master:
      - step:
          deployment: production
          script:
            - lftp -d -e "mirror --reverse . $FTP_TARGET_PATH" -p $FTP_PORT -u $FTP_USERNAME,$FTP_PASSWORD $FTP_TARGET_SITE
```

* Uses the an Alpine Linux image (super small), which already contains lftp.
* Skips `default`, which would deploy every commit, not just those on master.
* Uses `branches`/`master` with `deployment: production` to deploy only when changes are committed/merged to master and signals the production environment for deployments.
